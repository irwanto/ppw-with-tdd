from django.test import Client
from django.test import TestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class Story8UnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/story_8/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_function(self):
        found = resolve('/story_8/')
        self.assertEqual(found.func, index)

    def test_landing_page_using_index_template(self):
        response = Client().get('/story_8/')
        self.assertTemplateUsed(response, 'index_story_8.html')

class Story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.browser.implicitly_wait(25)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_theme_is_changed(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_8/')
        time.sleep(3)
        old_theme = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
        tombol = self.browser.find_element_by_name('ubah_tema')
        tombol.send_keys(Keys.RETURN)
        new_theme = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
        time.sleep(3)
        self.assertNotEqual(old_theme, new_theme);

    def test_accordion_is_showed_and_closed(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_8/')
        time.sleep(3)
        acc = self.browser.find_element_by_class_name("accordion");
        acc.click()
        self.assertTrue("closed" not in acc.get_attribute("class"))
        time.sleep(3)
        acc.click()
        self.assertTrue("closed" in acc.get_attribute("class"))
        time.sleep(3)
