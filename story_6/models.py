from django.db import models
from django.utils import timezone

class Status(models.Model):
    status = models.CharField(max_length = 300)
    time = models.DateTimeField(default=timezone.now)
