# Generated by Django 2.1.1 on 2018-10-11 02:23

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('story_6', '0002_auto_20181011_0916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
