from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('add_status', views.add_status, name="add_status"),
    path('profile', views.show_profile, name="show_profile")
]
