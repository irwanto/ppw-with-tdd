from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, show_profile
from .models import Status

import time

class Story6UnitTest(TestCase):
    def test_story_6_landing_page_url_is_exist(self):
        response = Client().get('/story_6/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_landing_page_using_index_template(self):
        response = Client().get('/story_6/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story_6_landing_page_using_index_func(self):
        found = resolve('/story_6/')
        self.assertEqual(found.func, index)

    def test_story_6_landing_page_has_greeting_text(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        self.assertIn("Hello, apa kabar?", html_response)

    def test_form_post_success_and_save_to_model(self):
        response_post = Client().post('/story_6/add_status', {'status': 'Stres PPW', 'time': timezone.now})
        self.assertEqual(response_post.status_code, 302)

        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_form_show_result(self):
        Client().post('/story_6/add_status', {'status': 'Stres PPW', 'time': timezone.now})
        response = Client().get('/story_6/')
        html_response = response.content.decode('utf8')
        self.assertIn('Stres PPW', html_response)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(time=timezone.now(), status='tes')
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

class Challenge6UnitTest(TestCase):
    def test_profile_page_url_is_exist(self):
        response = Client().get('/story_6/profile')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_show_profile_function(self):
        found = resolve('/story_6/profile')
        self.assertEqual(found.func, show_profile)

    def test_profile_page_using_profile_template(self):
        response = Client().get('/story_6/profile')
        self.assertTemplateUsed(response, 'profile.html')

class Story7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_input_form_status(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_6/')
        time.sleep(3)
        status_box = self.browser.find_element_by_name('status')
        status_box.send_keys('Coba Coba')
        time.sleep(3)
        submit = self.browser.find_element_by_class_name('btn-primary')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn("Coba Coba", self.browser.page_source)

    def test_hello_and_form_is_displayed(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_6/')
        hello = self.browser.find_element_by_tag_name('h1')
        self.assertTrue(hello.is_displayed())
        form = self.browser.find_element_by_id('form')
        self.assertTrue(form.is_displayed())

    def test_form_is_aligned_center(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_6/')
        time.sleep(3)
        div = self.browser.find_element_by_tag_name('div')
        self.assertTrue(div.get_property("align") == 'center')

    def test_hello_font(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_6/')
        hello = self.browser.find_element_by_tag_name('h1')
        self.assertTrue(hello.value_of_css_property('font'), 'Segoe UI')

    def test_button_color_is_blue(self):
        self.browser.get('http://ppw-irwanto-with-tdd.herokuapp.com/story_6/')
        button = self.browser.find_element_by_tag_name('button')
        self.assertTrue(button.value_of_css_property('color'), 'blue')
