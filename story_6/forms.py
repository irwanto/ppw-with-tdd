from django import forms

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control',
        'type': 'textarea',
        'style': 'width: 80%',
        'rows': 4,
        'placeholder': 'Masukkan status baru',
    }

    status = forms.CharField(required=True, max_length=300, widget=forms.Textarea(attrs=attrs))
