from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

def index(request):
    form = StatusForm()
    all_status = Status.objects.all()
    return render(request, 'index.html', {'form': form, 'all_status': all_status})

def add_status(request):
    form = StatusForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        status = request.POST['status']

        new_status = Status(status=status)
        new_status.save()
        
    return HttpResponseRedirect('/story_6/')

def show_profile(request):
    return render(request, 'profile.html', {})
