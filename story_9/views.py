from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from google.oauth2 import id_token
from google.auth.transport import requests as oauthrequests

import json
import requests

CLIENT_ID = '581287295509-qs0o85rfivhlektd9kf0sntmfvuiptns.apps.googleusercontent.com'

@csrf_exempt
def index(request):
    if 'username' not in request.session:
        return HttpResponseRedirect(reverse('login'))

    if request.method == 'POST':
        id = request.POST.get('id')
        books = request.session['favBooks']
        if id not in books:
            books.append(id)
            request.session['favBooks'] = books
            count = len(books)
            status = 'add'
        else:
            books.remove(id)
            request.session['favBooks'] = books
            count = len(books)
            status = 'remove'

        return JsonResponse({'count': count, 'status': status})
    response = {}
    response['numOfFavBooks'] = len(request.session['favBooks'])
    response['username'] = request.session['username']
    return render(request, 'index_story_9.html', response)

@csrf_exempt
def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))

@csrf_exempt
def login(request):
    if request.method == "POST":
        try:
            token = request.POST.get('token')
            idinfo = id_token.verify_oauth2_token(token, oauthrequests.Request(), CLIENT_ID)

            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            userid = idinfo['sub']
            username = idinfo['name']
            useremail = idinfo['email']
            userpic = idinfo['picture']

            request.session['user_id'] = userid
            request.session['username'] = username
            request.session['email'] = useremail
            request.session['picture'] = userpic
            request.session['favBooks'] = []

            return JsonResponse({"status": "oke"})
        except ValueError:
            return JsonResponse({"status": "fail"})
    return render(request, 'login.html', {})

@csrf_exempt
def get_book_list(request, toSearch):
    get_from_gbooks = requests.get("https://www.googleapis.com/books/v1/volumes?q="+toSearch)
    all_books_data = get_from_gbooks.json()

    book_list = []
    for book in all_books_data['items']:
        new_book = {
            'id' : '',
            'title' : '',
            'authors' : '-',
            'published_date' : 'Unknown',
        }
        if 'title' in book['volumeInfo']:
            new_book['title'] = book['volumeInfo']['title']
        if 'authors' in book['volumeInfo']:
            new_book['authors'] = ', '.join([author for author in book['volumeInfo']['authors']])
        if 'publishedDate' in book['volumeInfo']:
            new_book['published_date'] = book['volumeInfo']['publishedDate']
        if 'id' in book:
            new_book['id'] = book['id']

        book_list.append(new_book)

    dataJson = json.dumps({"book_list": book_list})
    mimetype = 'application/json'

    return HttpResponse(dataJson, mimetype)
