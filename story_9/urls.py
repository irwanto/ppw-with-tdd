from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('get_book_list/<str:toSearch>/', views.get_book_list, name="get_book_list"),
]
