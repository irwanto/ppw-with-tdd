from django.test import Client
from django.test import TestCase
from django.urls import resolve
from .views import index, get_book_list, login

import requests

class Story9UnitTest(TestCase):
    def setUp(self):
        user = self.client.session
        user['username'] = 'user'
        user['favBooks'] = ['dummy']
        user.save()

    def test_user_is_not_signed_in(self):
        response = Client().get('/story_9/')
        self.assertEqual(response.status_code, 302)

    def test_landing_page_url_is_exist(self):
        response = self.client.get('/story_9/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_function(self):
        found = resolve('/story_9/')
        self.assertEqual(found.func, index)

    def test_landing_page_using_index_template(self):
        response = self.client.get('/story_9/')
        self.assertTemplateUsed(response, 'index_story_9.html')

    def test_add_fav_book(self):
        response = self.client.post('/story_9/', {'id': 'buku'})
        self.assertEqual(len(self.client.session['favBooks']), 2)
        self.assertEqual(response.status_code, 200)

    def test_remove_fav_book(self):
        response = self.client.post('/story_9/', {'id': 'dummy'})
        self.assertEqual(len(self.client.session['favBooks']), 0)
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        response = self.client.get('/logout')
        self.assertEqual(response.status_code, 302)

    def test_login_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_function(self):
        found = resolve('/login')
        self.assertEqual(found.func, login)

    def test_login_page_using_login_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'login.html')

    # def test_login_success(self):
    #     response = Client().post('/login', {'token': 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNTgxMjg3Mjk1NTA5LXFzMG84NXJmaXZobGVrdGQ5a2Ywc250bWZ2dWlwdG5zLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNTgxMjg3Mjk1NTA5LXFzMG84NXJmaXZobGVrdGQ5a2Ywc250bWZ2dWlwdG5zLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA0NDQ5MzIyMTE2MDM4MDkyNDUzIiwiZW1haWwiOiJzYW5naGFkYW5ha21idWkxOEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkgxbDJhc3QxMjFscWtfYTNYVHZ3dnciLCJuYW1lIjoic2FuZ2hhZGFuYSBrbWJ1aTE4IiwicGljdHVyZSI6Imh0dHBzOi8vbGg0Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tbkJOT21yektoa3cvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUdEZ3ctaG5TVXZFNVdBY08xSDJPdG93UUhrUTF4MDVVQS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoic2FuZ2hhZGFuYSIsImZhbWlseV9uYW1lIjoia21idWkxOCIsImxvY2FsZSI6ImlkIiwiaWF0IjoxNTQzNDU5NDkzLCJleHAiOjE1NDM0NjMwOTMsImp0aSI6IjlhOWJkZjNmMmVhYjk1OTY4MzkzMzNhZDQ5YzJjMjQ1NTk5ODAwYzUifQ.bN5Jskc9JrBSua1GLvUfkUlcpiBGY2ybIuWyLdhhLbaM5x0mo8ku_RCMtMqSnKF5YhnVhOm7mR8pC5R5t5oSxnyOj8KGoCvYeXPY8tkkrkijVCOlSPZZ1JioeYzvcI0BGXKfbxf1oOP1QCyVFlaRmrEKFMuBkiIil2WB0vw6-fVSj4oH5AJ9dO7tCQx9-_XjPeeng7p4szEuNBNXAjbxvHDZjx8tlqjvyx5j65cN_xGsx38tCRlKE_0ifflLcpGtvvfujXSLiX0EXEXfILcXRZLSHG2BgH-W9-bHCsbh4gEJiWGK3jROyJPf7lUXZZUw2ljGY5SmoQAvsPru63A4cg'})
    #     self.assertEqual(response.status_code, 200)
    #     # self.assertEqual(response.client.session['username'], 'sanghadana kmbui18')
    #     # self.assertEqual(response.client.session['email'], 'sanghadanakmbui18@gmail.com')


    def test_login_account_not_exist(self):
        response = Client().post('/login', {'token': 'wrongtoken'})
        self.assertRaises(ValueError)
        self.assertRaisesMessage(ValueError, 'Wrong issuer.')

    def test_get_book_list_url_is_exist(self):
        response = Client().get('/story_9/get_book_list/test')
        self.assertEqual(response.status_code, 301)
