var toSearch = "quilting";

$(document).ready(function () {
    // $.ajax({
    //     method: "GET",
    //     url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
    //     success: function (result) {
    //         console.log(result)
    //         var arrayOfBooks = result.items;
    //         for (var i = 0; i < arrayOfBooks.length; i++) {
    //             var button = '<button class="favbutton"><i class="favstar"></i></button>'
    //             var toAdd = '<tr>' + '<td>' + arrayOfBooks[i].volumeInfo.title + '</td>' + '<td>' + arrayOfBooks[i].volumeInfo.authors + '</td>' +
    //                 '<td>' + arrayOfBooks[i].volumeInfo.publishedDate + '</td>' + '<td>' + button + '</td>' + '</tr>';
    //             $('tbody').append(toAdd);
    //         }
    //     }
    // });

    $.ajax({
        method: "GET",
        url: "/story_9/get_book_list/" + toSearch + "/",
        success: function (result) {
            fillTable(result);
        },
        error: function(error) {
            console.log("failed ._.")
        }
    });
});

function fillTable(result) {
    console.log(result);
    $("#table-content").empty();

    var arrayOfBooks = result.book_list;
    for (var i = 0; i < arrayOfBooks.length; i++) {
        var button = '<button class="favbutton" id="' + arrayOfBooks[i].id +'" onclick=addRemoveFavorite("' + arrayOfBooks[i].id + '")><i class="favstar"></i></button>'
        var toAdd = '<tr>' + '<td>' + arrayOfBooks[i].title + '</td>' + '<td>' + arrayOfBooks[i].authors + '</td>' +
            '<td>' + arrayOfBooks[i].published_date + '</td>' + '<td>' + button + '</td>' + '</tr>';
        $('tbody').append(toAdd);
    }
}

function addRemoveFavorite(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    console.log(id);
    $.ajax({
        method: "POST",
        url: "/story_9/",
        data: {
            'id': id,
        },
        headers: {
            "X-CSRFToken": csrftoken
        },
        success: function(result) {
            if (result.status=='remove') {
                $('#'+id).find(".favstar").css("content", "url('../static/story_9/images/blank_star.png')");
                // $('#'+id).removeClass("selected");
            } else if (result.status=='add'){
                $('#'+id).find(".favstar").css("content", "url('../static/story_9/images/star.png')");
                // $('#'+id).addClass("selected");
            }
            $('#numOfFavBooks').html(result.count);
        }
    });
}

function searchBook() {
    toSearch = document.getElementById("search-box").value;
    console.log(toSearch);
    $.ajax({
        type: "GET",
        url: "/story_9/get_book_list/" + toSearch + "/",
        success: function (result) {
            fillTable(result);
        },
        error: function(error) {
            console.log("failed ._.")
        }
    })
}
