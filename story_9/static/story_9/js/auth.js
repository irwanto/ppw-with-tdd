$(document).ready(function () {
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
});

function onSignIn(googleUser) {
    console.log(googleUser.getBasicProfile().getName() + " sukses sign in pake google yeay");
    var id_token = googleUser.getAuthResponse().id_token;

    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/login",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {
            'token': id_token
        },
        success: function(result) {
            if (result.status == 'oke') {
                console.log("sukses kasih ke backend");
                window.location.href = "/story_9/";
            } else {
                alert("Something went wrong");
            }
        },
    });
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');

        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/logout",
            headers: {
                "X-CSRFToken": csrftoken
            },
            complete: function() {
                window.location.href = "/login"
            }
        });
    });
}
