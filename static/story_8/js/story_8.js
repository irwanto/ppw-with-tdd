var i = 0;

color1 = ["black", "white", "green", "yellow"];
color2 = ["white", "black", "yellow", "green"];
color3 = ["grey", "cyan"];

function ubah_tema() {
    i++;
    document.body.style.backgroundColor = color1[i % color1.length];
    document.body.style.color = color2[i % color2.length];
    all = document.getElementsByClassName("ubah-tema");
    //all = all + all = document.getElementsByClassName("accordion");
    for (j = 0; all.length; j++) {
        all[j].style.backgroundColor = color2[i % color2.length];
        all[j].style.color = color1[i % color1.length];
    }
}

function preview_tema() {
    document.body.style.backgroundColor = color1[(i+1) % color1.length];
    document.body.style.color = color2[(i+1) % color2.length];
    all = document.getElementsByClassName("ubah-tema");
    for (j = 0; all.length; j++) {
        all[j].style.backgroundColor = color2[(i+1) % color2.length];
        all[j].style.color = color1[(i+1) % color1.length];
    }
}

function tema_awal() {
    document.body.style.backgroundColor = color1[i % color1.length];
    document.body.style.color = color2[i % color2.length];
    all = document.getElementsByClassName("ubah-tema");
    for (j = 0; all.length; j++) {
        all[j].style.backgroundColor = color2[i % color2.length];
        all[j].style.color = color1[i % color1.length];
    }
}

function bar() {
    var bar = document.getElementById("load-bar");
    var width = 1;
    var id = setInterval(load, 20);
    function load() {
        if (width >= 80) {
            clearInterval(id);
        } else {
            width++;
            bar.style.width = width + '%';
        }
    }

    setTimeout(showPage, 2000);
}

function showPage() {
    document.getElementsByClassName("loading")[0].style.display = "none";
    document.getElementsByClassName("content")[0].style.display = "block";
}

$(function(){
    var allPanels = $(".panel").hide();

    $(".accordion").click(function(){
        if ($(this).hasClass("closed")) {
            $(this).find(".panel").slideDown();
            $(this).removeClass("closed");
        } else {
            $(this).addClass("closed");
            $(this).find(".panel").slideUp();
        }
    });

    $(".accordion").mouseenter(function(){
        if (($(this).css("background-color") == "rgb(255, 255, 255)") || ($(this).css("background-color") == "rgb(0, 0, 0)") || ($(this).css("background-color") == "rgb(128, 128, 128)")) {
            var acc_color = color3[0];
        } else {
            var acc_color = color3[1];
        }
        $(this).css("background-color", acc_color);
    });

    $(".accordion").mouseleave(function(){
        $(this).css("background-color", color2[i % color2.length]);
    });
 });
