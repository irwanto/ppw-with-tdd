var toSearch = "quilting";
var numOfFavBooks = 0;

$(document).ready(function () {
    // $.ajax({
    //     method: "GET",
    //     url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
    //     success: function (result) {
    //         console.log(result)
    //         var arrayOfBooks = result.items;
    //         for (var i = 0; i < arrayOfBooks.length; i++) {
    //             var button = '<button class="favbutton"><i class="favstar"></i></button>'
    //             var toAdd = '<tr>' + '<td>' + arrayOfBooks[i].volumeInfo.title + '</td>' + '<td>' + arrayOfBooks[i].volumeInfo.authors + '</td>' +
    //                 '<td>' + arrayOfBooks[i].volumeInfo.publishedDate + '</td>' + '<td>' + button + '</td>' + '</tr>';
    //             $('tbody').append(toAdd);
    //         }
    //     }
    // });

    $.ajax({
        method: "GET",
        url: "/story_9/get_book_list/" + toSearch + "/",
        success: function (result) {
            fillTable(result);
            addRemoveFavorite();
        },
        error: function(error) {
            console.log("failed ._.")
        }
    });
});

function fillTable(result) {
    console.log(result);
    $("#table-content").empty();

    var arrayOfBooks = result.book_list;
    for (var i = 0; i < arrayOfBooks.length; i++) {
        var button = '<button class="favbutton"><i class="favstar"></i></button>'
        var toAdd = '<tr>' + '<td>' + arrayOfBooks[i].title + '</td>' + '<td>' + arrayOfBooks[i].authors + '</td>' +
            '<td>' + arrayOfBooks[i].published_date + '</td>' + '<td>' + button + '</td>' + '</tr>';
        $('tbody').append(toAdd);
    }
}

function addRemoveFavorite() {
    numOfFavBooks = 0;
    $(".favbutton").click(function(){
        if ($(this).hasClass("selected")) {
            $(this).find(".favstar").css("content", "url('../static/story_9/images/blank_star.png')");
            $(this).removeClass("selected");
            numOfFavBooks--;
            $("#numOfFavBooks").html(numOfFavBooks);
        } else {
            $(this).find(".favstar").css("content", "url('../static/story_9/images/star.png')");
            $(this).addClass("selected");
            numOfFavBooks++;
            $("#numOfFavBooks").html(numOfFavBooks);
        }
    });
}

function searchBook() {
    toSearch = document.getElementById("search-box").value;
    console.log(toSearch);
    $.ajax({
        type: "GET",
        url: "/story_9/get_book_list/" + toSearch + "/",
        success: function (result) {
            fillTable(result);
            addRemoveFavorite();
        },
        error: function(error) {
            console.log("failed ._.")
        }
    })
}
