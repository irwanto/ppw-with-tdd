from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import SubscribeForm
from .models import Subscriber

import json

@csrf_exempt
def validate_email(request):
    email = request.POST.get('email')
    data = {
        'is_taken': Subscriber.objects.filter(email=email).exists()
    }
    return JsonResponse(data)

@csrf_exempt
def subscribe(request):
    if request.method=="POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        password = request.POST.get('password')
        new_subscriber = Subscriber(name=name, email=email, password=password)
        new_subscriber.save()
        data = {
            'success': True
        }
        return JsonResponse(data)

    form = SubscribeForm()
    return render(request, 'index_story_10.html', {'form': form})

@csrf_exempt
def subscriber_list(request):
    if request.method=="POST":
        email = request.POST.get('email')
        password = request.POST.get('password')
        toDelete = Subscriber.objects.get(pk=email)
        delete = False
        if (toDelete.password == password):
            delete = True
            toDelete.delete()
        return JsonResponse({'delete_success': delete})

    all_subscriber = Subscriber.objects.all().values("name", "email")
    dataJson = json.dumps({'subscriber_list': list(all_subscriber)})
    return HttpResponse(dataJson, 'application/json')
