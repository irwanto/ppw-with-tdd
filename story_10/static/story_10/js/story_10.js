$(document).ready(function() {
    fillTable();
});

function fillTable(){
    $.ajax({
        method: "GET",
        url: "/story_10/subscriber_list",
        success: function (result) {
            console.log(result);
            $("#table-content").empty();

            var listOfSubscriber = result.subscriber_list;
            for (var i = 0; i < listOfSubscriber.length; i++) {
                var button = '<button class="btn btn-primary unsubscribe-btn" onclick=deleteSubscriber(\'' + listOfSubscriber[i].email + '\')>Unsubscribe</button>'
                var toAdd = '<tr>' + '<td>' + listOfSubscriber[i].name + '</td>' + '<td>' + listOfSubscriber[i].email + '</td>' +
                            '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(toAdd);
            }
        },
        error: function(error) {
            console.log("failed ._.")
        }
    });
}

function deleteSubscriber(email) {
    var pass_confirmation = prompt("Confirm with your password", "");
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/story_10/subscriber_list",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {
            'email': email,
            'password': pass_confirmation,
        },
        success: function(result) {
            if (result.delete_success) {
                alert('Unsubscribe successful');
                fillTable();
            } else {
                alert('Wrong password');
            }
        }
    });
}

function validateInput() {
    var input = document.getElementById("subscribe-form");
    var input_name = input.elements[1].value;
    var input_email = input.elements[2].value;
    var input_password = input.elements[3].value;
    var submit_button = document.getElementById("subscribe-btn");
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "../story_10/validate_email",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {
            'email': input_email,
        },
        dataType: "json",
        success: function(data) {
            if ((input_email == "") || (input_name == "") || (input_password == "")) {
                submit_button.disabled = true;
            }
            else if (data.is_taken) {
                submit_button.disabled = true;
                alert("Email is already registered before")
            } else {
                submit_button.disabled = false;
            }
        },
        error: function(error) {
            console.log("error :(");
        }
    });
}

function addSubscriber() {
    event.preventDefault(true);
    var input = document.getElementById("subscribe-form");
    var input_name = input.elements[1].value;
    var input_email = input.elements[2].value;
    var input_password = input.elements[3].value;
    var submit_button = document.getElementById("subscribe-btn");
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "../story_10/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {
            'name': input_name,
            'email': input_email,
            'password': input_password,
        },
        dataType: "json",
        success: function(data) {
            alert("Subscription success")
            submit_button.disabled = true;
            for (var i = 0; i < input.length; i++) {
                input.elements[i].value = "";
            }
            fillTable();
        }
    });
}
