from django.urls import path
from . import views

urlpatterns = [
    path('', views.subscribe, name="subscribe"),
    path('validate_email', views.validate_email, name="validate_email"),
    path('subscriber_list', views.subscriber_list, name="subscriber_list"),
]
