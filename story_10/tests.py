from django.test import Client
from django.test import TestCase
from django.urls import resolve
from .views import subscribe
from .models import Subscriber

class Story10UnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/story_10/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_function(self):
        found = resolve('/story_10/')
        self.assertEqual(found.func, subscribe)

    def test_landing_page_using_index_template(self):
        response = Client().get('/story_10/')
        self.assertTemplateUsed(response, 'index_story_10.html')

    def test_email_validation_not_exist(self):
        response = Client().post('/story_10/validate_email', {'email': 'tes@mail.com'})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.json()['is_taken'])

    def test_email_validation_exist(self):
        Subscriber.objects.create(name="Test", email="test@email.com", password="12345678")
        response = Client().post('/story_10/validate_email', {'email': 'test@email.com'})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json()['is_taken'])

    def test_subcsribe(self):
        response = Client().post('/story_10/', {'name': 'tes', 'email': 'tes@mail.com', 'password': '123456'})
        self.assertEqual(response.status_code, 200)
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)

    def test_get_subscriber_list(self):
        response = Client().get('/story_10/subscriber_list')
        self.assertEqual(response.status_code, 200)

    def test_unsubscribe(self):
        Subscriber.objects.create(name="Test", email="test@email.com", password="12345678")
        response = Client().post('/story_10/subscriber_list', {'email': 'test@email.com', 'password': '123456'})
        self.assertEqual(response.status_code, 200)
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)

        response = Client().post('/story_10/subscriber_list', {'email': 'test@email.com', 'password': '12345678'})
        self.assertEqual(response.status_code, 200)
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 0)

class Story10ModelUnitTest(TestCase):
    def test_can_create_subscriber_model(self):
        new_subscriber = Subscriber.objects.create(name="Test", email="test@email.com", password="12345678")
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)
