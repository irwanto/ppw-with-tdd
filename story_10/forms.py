from django import forms

class SubscribeForm(forms.Form):
    name = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Your name..',
        'onblur': 'validateInput()',
    }))
    email = forms.EmailField(required=True, max_length=100, widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Your email..',
        'onblur': 'validateInput()',
    }))
    password = forms.CharField(required=True, max_length=100, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Your password..',
        'onblur': 'validateInput()',
    }))
